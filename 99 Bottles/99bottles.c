#include <stdio.h>

void bottles_of_beer(int num) {
  for (int i = num; i >= 0; i--) {
    if (i == 2) {
      printf("%d bottles of beer on the wall, %d bottles of beer.\nTake one down and pass it around, %d bottle of beer on the wall.\n\n", i, i, i - 1);
    } else if (i == 1) {
      printf("%d bottles of beer on the wall, %d bottles of beer.\nTake one down and pass it around, no more bottles of beer on the wall.\n\n", i, i, i - 1);
    } else if (i == 0) {
      printf("No more bottles of beer on the wall, no more bottles of beer.\nGo to the store and buy some more, %d bottles of beer on the wall.\n", num);
    } else {
      printf("%d bottles of beer on the wall, %d bottles of beer.\nTake one down and pass it around, %d bottles of beer on the wall.\n\n", i, i, i - 1);
    }
  }
}

int main(void) {
  int numBottles = -1;
  printf("How many bottles? ");
  scanf("%d", &numBottles);
  if (numBottles < 0) {
    return -1;
  }
  printf("\n");
  bottles_of_beer(numBottles);
}
